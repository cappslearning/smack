//
//  Channel.swift
//  Smack
//
//  Created by Marius Caciulescu on 12/29/17.
//  Copyright © 2017 iOSLearning-Devslopes. All rights reserved.
//

import Foundation

struct Channel {
    
    public private (set) var channelTitle: String!
    public private (set) var channelDescription: String!
    public private (set) var id: String!
}
